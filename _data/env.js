const environment = process.env.ELEVENTY_ENV
const PROD_ENV = 'production'
const prodUrl = 'https://steinbeck.bob-humphrey.com'
const devUrl = 'http://localhost:8080'
const baseUrl = environment === PROD_ENV ? prodUrl : devUrl
const isProd = environment === PROD_ENV
const currentYear = new Date().getFullYear()

module.exports = {
  title: 'John Steinbeck Scrapbook',
  author: 'Bob Humphrey',
  description: 'Information about the life and work of'
    + 'Nobel prize winning novelist '
    + 'John Steinbeck.',
  environment,
  isProd,
  baseUrl,
  currentYear
}